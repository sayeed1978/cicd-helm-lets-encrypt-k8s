# Example CI/CD project with Helm and auto LetsEncrypt

This repository contains a Go programs of a web server that answers the question: "Is Go 1.x out yet?"

## Dependencies/Requirements
- Git
- Go / golang
- Docker
- Kubernetes (or Kind/Minikube)
- Helm
- Kubectl
- Ansible
- python
- kubernetes
- PyYAML
- jsonpatch


## Clone the project

```
$ git clone git@gitlab.com:sayeed1978/cicd-helm-lets-encrypt-k8s.git
```

## Edit and helm/isoutyet/values.yaml with email address

```
$ cd cicd-helm-lets-encrypt-k8s
$ vim helm/isoutyet/values.yaml
```
## Create namespace "outyet" for the project

```
$ kubectl create namespace outyet
```

## Deployment with ansible playbook

### Create Ingress Controller with helm charts

```
$ ansible-playbook ansible/playbboks/create-ingress-controller.yaml
```

`Run the following command and note the load balancer's EXTERNAL-IP. Configure/add a DNS A record with it. Update helm/isoutyet/values.yaml file with the DNS hostname.`

```
$ kubectl get service ingress-nginx-controller -n outyet
```

### Deploy letsencrypt helm charts

```
$ ansible-playbook ansible/playbboks/deploy-lets-encryot.yaml
```

### Trigger CI/CD pipeline. If successfull open a browser and test the ingress configuration with the URL

### Optionally run the local app chart playbook to deploy the app

```
$ ansible-playbook ansible/playbboks/deploy-outyet-app-helm-chart.yaml
```

## Manual Deployment

### Create a namespace "outyet" for the project

```
$ kubectl create namespace outyet
```

## Create an ingress controller

### Add ingress-nginx Helm repository

```
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
```

### Use Helm to deploy an NGINX ingress controller

```
helm install ingress-nginx ingress-nginx/ingress-nginx \
    --namespace outyet \
    --set controller.replicaCount=2
```

### Note the public IP from ingress controller load balancer

```
kubectl get all -n outyet
```

`Add/configure a DNS A record with the EXTERNAL-IP displaed`

## Configure LetsEncrypt

`Label the cert-manager namespace to disable resource validation`

```
kubectl label namespace outyet cert-manager.io/disable-validation=true
```

### Add the Jetstack Helm repository

```
helm repo add jetstack https://charts.jetstack.io
```

### Update your local Helm chart repository cache

```
helm repo update
```

### Install CRDs with kubectl

```
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.7.1/cert-manager.crds.yaml
```

### Install cert-manager Helm chart

```
helm install cert-manager jetstack/cert-manager \
  --namespace outyet \
  --version v1.7.1
```

## Deploy outyet app with the helm templates

```
cd ../helm/isoutyet
helm upgrade --install trc-test . --namespace outyet
```

## Verify certificate if created successfully

```
kubectl describe certificate tls-secret --namespace outyet
```

## Open a browser and test the ingress configuration with the URL

## Gitlab configuration for Deploy stage

`Create base64 string from the config and put it into kube_config (or any name you prefer) variable.` 

`From Project > Settings > CI/CD > Variables`

```
cat ~/.kube/config | base64
```

`Create kubernetes cluster agent from Infrastructure > Kubernetes Cluster > Connect a cluster. Ignore applying config to the k8s cluster. The agent does not need to be connected`

## Building the outyet binary

```
$ cd cicd-helm-lets-encrypt-k8s/app/outyet
$ go build .
```

OR

```
$ go build -o /path/to/BINARY_NAME
```